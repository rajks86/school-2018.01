from ..git import *
from ..filter import GitRepositoryDownloader

def test_with_login():
    git = Git("Andrewerr", "Qazwsx12")
    assert git.search_repository("a") != None

def test_no_login():
    git = Git()
    assert git.search_repository("a") != None

def test_zero_result():
    git = Git()
    assert git.search_repository("afklvmfkjgkljbokjritjhgkjrthgkjrthghkhjkrngkrnkjhgkjhgkjhrtkjghkjtrhg") == []

def test_downloader():
    filter = GitRepositoryDownloader("http://github.com/PavelOstyakov/dist-fs")
    assert filter.run()
