from ...search_results_sorter import SearchResultsSorter

def test_output():
    config = {"response_size": 2}
    files_info = [
        ("url1", "Hello world!"),
        ("url2", "Hello Hello Hello world! world! world!"),
        ("url3", "It is wednesday, my dude"),
        ("url4", "Hello dude")
    ]
    response = SearchResultsSorter(files_info, config)
    query = "Hello world!"
    result = response.sort_by_relevance(query)
    assert result == [files_info[1], files_info[0]]
